package hu.braininghub.bh06.bookstore.web;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh04.api.builder.container.HtmlDocument;
import hu.braininghub.bh04.api.builder.container.Table;
import hu.braininghub.bh04.api.builder.controller.Paraghraph;
import hu.braininghub.bh06.bookstore.dao.BookDAO;
import hu.braininghub.bh06.bookstore.dao.DefaultBookDAO;
import hu.braininghub.bh06.bookstore.dto.BookDTO;
import hu.braininghub.bh06.bookstore.service.BookService;
import hu.braininghub.bh06.bookstore.service.DefaultBookService;
import hu.braininghub.bh06.bookstore.service.ServiceLocator;

@WebServlet(urlPatterns = "/books")
public class BookListServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		List<BookDTO> books = ServiceLocator.getBookService().getBooks();

		HtmlDocument doc = new HtmlDocument();

		String[] headers = new String[] { "Title", "Author", "Isbn", "Year", "Genre", "Price", "Available number" };

		Table t = new Table("books", books.size(), headers.length, headers, doc);

		for (int i = 0; i < books.size(); i++) {

			for (int j = 0; j < headers.length; j++) {

				Object value = null;
				BookDTO book = books.get(i);

				if (j == 0) {
					value = book.getTitle();
				} else if (j == 1) {
					value = book.getAuthor();
				} else if (j == 2) {
					value = book.getIsbn();
				} else if (j == 3) {
					value = book.getYear();
				} else if (j == 4) {
					value = book.getGenre().name();
				} else if (j == 5) {
					value = book.getPrice();
				} else if (j == 6) {
					value = book.getAvailableNum();
				}
				t.addElementToIndex(i, j, new Paraghraph(UUID.randomUUID().toString(), value, t));
			}
		}

		doc.addChild(t);
		resp.getWriter().println(doc.getHTMLCodeAsString());
	}

}
