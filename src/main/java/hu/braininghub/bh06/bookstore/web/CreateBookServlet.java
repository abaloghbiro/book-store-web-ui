package hu.braininghub.bh06.bookstore.web;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh04.api.builder.container.Form;
import hu.braininghub.bh04.api.builder.container.HtmlDocument;
import hu.braininghub.bh04.api.builder.controller.Input;
import hu.braininghub.bh04.api.builder.controller.InputTypes;
import hu.braininghub.bh06.bookstore.dao.BookDAO;
import hu.braininghub.bh06.bookstore.dao.DefaultBookDAO;
import hu.braininghub.bh06.bookstore.dto.BookDTO;
import hu.braininghub.bh06.bookstore.model.Genre;
import hu.braininghub.bh06.bookstore.service.BookService;
import hu.braininghub.bh06.bookstore.service.DefaultBookService;
import hu.braininghub.bh06.bookstore.service.ServiceLocator;

@WebServlet(urlPatterns = "/createbook")
public class CreateBookServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HtmlDocument doc = new HtmlDocument();

		String path = req.getContextPath() + "/createbook";
		Form bookForm = new Form("newBook", path, doc);

		bookForm.addChild(new Input(InputTypes.text, "title", "title", "", bookForm));
		bookForm.addChild(new Input(InputTypes.text, "author", "author", "", bookForm));
		bookForm.addChild(new Input(InputTypes.text, "isbn", "isbn", "", bookForm));
		bookForm.addChild(new Input(InputTypes.text, "year", "year", "", bookForm));
		bookForm.addChild(new Input(InputTypes.text, "genre", "genre", "", bookForm));
		bookForm.addChild(new Input(InputTypes.text, "price", "price", "", bookForm));
		bookForm.addChild(new Input(InputTypes.text, "availableNum", "availableNum", "", bookForm));

		doc.addChild(bookForm);

		resp.getWriter().println(doc.getHTMLCodeAsString());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String title = req.getParameter("title");
		String author = req.getParameter("author");
		String isbn = req.getParameter("isbn");
		String year = req.getParameter("year");
		String genre = req.getParameter("genre");
		String price = req.getParameter("price");
		String availableNum = req.getParameter("availableNum");

		BookDTO book = new BookDTO(title, author, isbn, Integer.parseInt(year), Genre.valueOf(genre),
				Double.parseDouble(price));
		book.setAvailableNum(Integer.parseInt(availableNum));

		ServiceLocator.getBookService().buyBook(book);

		String path = req.getContextPath() + "/createbook";
		resp.sendRedirect(path);
	}

}
